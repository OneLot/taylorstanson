<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'wordpressuser' );

/** MySQL database password */
define( 'DB_PASSWORD', 'password' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

define('FS_METHOD', 'direct');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'F}e(W&H<V[9Eh+8:f)(1-%p>SpS5iRH%4|wIr9%DF||++K9Q-a8XOKHn%]=IJu[O');
define('SECURE_AUTH_KEY',  'VZ@WLPmJLw!Xc%Ue[U_w(R9_?)#1G[l/4I!%o#<kU^>K5=!?rV#!/-6D@okcDo]y');
define('LOGGED_IN_KEY',    'MG4i{(PJS7qu0yP%~=]J@c==9@XplH|@^)S@0#T,P-B5uu*[AmW9N1-fJRdE,QJB');
define('NONCE_KEY',        'l`=GqaHZZ:^vy3kRb,#FrLgiV1m=3.rfEq0h}F68f3XlCz<x.4L)s&Cmb!Jpg#+N');
define('AUTH_SALT',        ' );1w?`KBE[hf~)`KE4Ef O[w;Pv +2+3I3%DwWwLN8W }dNHJ03^Vl0/~^Hemw%');
define('SECURE_AUTH_SALT', '}l]+qujtLY]s/`Z!E+{:yP^Doh ,?VBDU8XsNEM[;u^Ih`@|*c+g`b6Tc{MB)p5(');
define('LOGGED_IN_SALT',   '2+@oB4ISmba1!Z.GNVX#^nw&T1Y`>%hssRTz;bmJ:sP%,jM|}mS>}|i6A1lty$JF');
define('NONCE_SALT',       'n%|z%Ju1$HC?)#H9cN//j(ugQV|C1/`1[kRnL}X {_^?rW_6M1QJC)dc#%2eV(ec');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
